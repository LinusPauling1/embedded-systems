/*
 * GccApplication1.c
 *
 * Created: 3/25/2021 6:38:08 PM
 * Author : paul
 */ 

#define F_CPU 16000000UL

#include <avr/io.h>
#include <util/delay.h>

unsigned char iterator;

int main(void)
{
    DDRD = 0xFF;
    while (1) 
    {
		/*
			digitalPin0 => PD0
			digitalPin1 => PD1
			digitalPin2 => PD2
			digitalPin3 => PD3
			digitalPin4 => PD4
			digitalPin5 => PD5
			digitalPin6 => PD6
			digitalPin7 => PD7
		*/
		PORTD = iterator++;
		_delay_ms(100);
    }
}

